# README #

Proyecto de muestra de Airflow y pipeline de datos.

### Docker ###

Se debe de contar con docker previamente instalado para poder ejecutar. El comando de instalción es 

docker-compose up

Podemos acceder a la administracion con el usuario

user: airflow
password: airflow

La ruta de acceso es la siguiente

http://localhost:8080/

### Variables ###

Ariflow cuenta con su sección de variables

http://localhost:8080/variable/list/

Es necesario importar el archivo 

variables.json incluido en el repositorio

### DAG ###

El dag principal se encuentra en esta ruta

http://localhost:8080/tree?dag_id=zenpli_pipeline

Desde esta vista se puede hacer el trigger de el DAG para probar sus componentes