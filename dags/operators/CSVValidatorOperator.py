import requests
import csv
import numpy as np
import pandas as pd
from numpy import genfromtxt

import math

def validate_types(df, mlist, type):
    for key in mlist:
            dataType_to_validate = df.dtypes[key]
            if not dataType_to_validate == type:
                raise "Invalid type for float key: " + key + " value: " + dataType_to_validate

def validate_limit(mlist, limit):
    i = 0
    for key in mlist:
        i = i + 1
        if not key < limit:
            message = str(i) + " nulls count: " + str(key)
            raise  Exception("Invalid data column" + message)


class CSVValidator:

    def __init__(
        self, 
        mpath,
        mschema):
        self.path = mpath
        self.schema = mschema

    def validate_schema(self, destination):
        list_of_column_names = []
        
        with open(destination) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter = ',')
            
            for row in csv_reader:
        # adding the first row
                list_of_column_names.append(row)
                break 
            print("List of column names : ",list_of_column_names[0])

        if(not np.array_equal(self.schema, list_of_column_names[0])):
             raise "Invalid schema"

        print(np.array_equal(self.schema, list_of_column_names[0]))

    def validate_missing_values(self, destination):
        df_describe = pd.read_csv(
            destination,
            na_values=['nan', 'NaN', 'na'])

        validate_limit(df_describe.isna().sum(), 500000)

    def validate_data_types(self, destination):
        df_describe = pd.read_csv(
            destination,
            na_values=['nan', 'NaN', 'na'])

        float_list = [
            'cont_3',
            'cont_4',
            'disc_6',
            'cont_9',
            'cont_10',
            ]

        int_list = [
            'disc_5'
            ]

        validate_types(df_describe, float_list, np.float64);
        validate_types(df_describe, int_list, np.int64);

    def data_stadarization(self, destination, column):
        df = pd.read_csv(
            destination,
            na_values=['nan', 'NaN', 'na'])
        df_z_scaled = df.copy()
  
        df_z_scaled[column] = (df[column] - df[column].mean()) / df[column].std()

        print(df_z_scaled)

    def data_filter(self, destination, column, filter):
        df = pd.read_csv(
            destination,
            na_values=['nan', 'NaN', 'na'])
        df_filtered = df[df[column] == filter]

        print(df_filtered)


    def data_aggregation(self, destination, column):
        df = pd.read_csv(
            destination,
            na_values=['nan', 'NaN', 'na'])
        df_aggregated = df.groupby(by=column).sum()

        print(df_aggregated)

    def data_transformation(self, destination, colunm_x, column_y):
        df = pd.read_csv(
            destination,
            na_values=['nan', 'NaN', 'na'])

        df['transformed_data'] = df.apply(
            lambda row: row[colunm_x] ** row[colunm_x] + math.exp(row[column_y]), axis=1)

        print(df)


    def data_uniques(self, destination, colunm):
        df = pd.read_csv(
            destination,
            na_values=['nan', 'NaN', 'na'])

        df_uniques = df[colunm].unique()

        print(df_uniques)


    def download_file_from_google_drive(self, id, destination):
        def get_confirm_token(response):
            for key, value in response.cookies.items():
                if key.startswith('download_warning'):
                    return value
    
            return None

        def save_response_content(response, destination):
            CHUNK_SIZE = 32768

            with open(destination, "wb") as f:
                for chunk in response.iter_content(CHUNK_SIZE):
                    if chunk: # filter out keep-alive new chunks
                        f.write(chunk)

        URL = "https://docs.google.com/uc?export=download"

        session = requests.Session()

        response = session.get(URL, params = { 'id' : id }, stream = True)
        token = get_confirm_token(response)

        if token:
            params = { 'id' : id, 'confirm' : token }
            response = session.get(URL, params = params, stream = True)

        save_response_content(response, destination)    
