# airflow related
from airflow import models
from airflow import DAG
# other packages
from datetime import datetime, timedelta

from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator

# import operators from the 'operators' file
from operators import CSVValidatorOperator

from airflow.models import Variable

import os

import logging


default_dag_args = {
   'start_date': datetime(2021, 10, 4, 7),
    'email_on_failure': True,
    'email_on_retry': True,
    'project_id' : 'zenpli_app',
    'retries': 1,
    'catchup': False,
    #'on_failure_callback': notify_email,
    'retry_delay': timedelta(minutes=5),
}

mcsv = CSVValidatorOperator.CSVValidator(
            "HI",
            ['key_1', 'date_2', 'cont_3', 'cont_4', 'disc_5', 'disc_6', 'cat_7', 'cat_8', 'cont_9', 'cont_10']
            )

with models.DAG(
    dag_id='zenpli_pipeline',
    # Continue to run DAG once per day
    schedule_interval = timedelta(days=1),
    catchup = False,
    start_date=datetime(2021, 10, 4),
    default_args=default_dag_args) as dag:

    task_1 = PythonOperator(
        task_id='dowloand_archive',
        python_callable= mcsv.download_file_from_google_drive,
        op_args=[
            '1JFH7_ffjrvYhOXVtepsqORfxiKjJtD0j',
            '/opt/airflow/test.csv'
        ],
    )

    task_2 = PythonOperator(
        task_id='validate_schema',
        python_callable= mcsv.validate_schema,
        op_args=[
            '/opt/airflow/test.csv'
        ],
    )

    task_3 = PythonOperator(
        task_id='validate_missing_values',
        python_callable= mcsv.validate_missing_values,
        op_args=[
            '/opt/airflow/test.csv'
        ],
    )

    task_4 = PythonOperator(
        task_id='validate_data_types',
        python_callable= mcsv.validate_data_types,
        op_args=[
            '/opt/airflow/test.csv'
        ],
    )

    task_5 = PythonOperator(
        task_id='data_stadarization',
        python_callable= mcsv.data_stadarization,
        op_args=[
            '/opt/airflow/test.csv',
            Variable.get("standarization_column")
        ],
    )

    task_6 = PythonOperator(
        task_id='data_filter',
        python_callable= mcsv.data_filter,
        op_args=[
            '/opt/airflow/test.csv',
            Variable.get("filter_column"),
            Variable.get("filter_value")
        ],
    )

    task_7 = PythonOperator(
        task_id='data_aggregation',
        python_callable= mcsv.data_aggregation,
        op_args=[
            '/opt/airflow/test.csv',
            Variable.get("aggregated_column")
        ],
    )

    task_8 = PythonOperator(
        task_id='data_transformation',
        python_callable= mcsv.data_transformation,
        op_args=[
            '/opt/airflow/test.csv',
            Variable.get("transformation_column_x"),
            Variable.get("transformation_column_y")
        ],
    )

    task_9 = PythonOperator(
        task_id='data_uniques',
        python_callable= mcsv.data_uniques,
        op_args=[
            '/opt/airflow/test.csv',
            Variable.get("uniques_column")
        ],
    )


task_1 >> task_2
task_1 >> task_3
task_1 >> task_4

task_2 >> task_5
task_3 >> task_5
task_4 >> task_5

task_5 >> task_6
task_6 >> task_7
task_7 >> task_8
task_8 >> task_9


if __name__ == "__main__":
    dag.cli()
